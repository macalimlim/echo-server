# echo-server

> A server that echoes back what you gave it (along with a timestamp and a host)

## Setup

You need to have [leiningen](https://leiningen.org/) and [JDK](https://jdk.java.net/) installed

Clone this project
```sh
$ git clone https://gitlab.com/macalimlim/echo-server
```

Configure the project for local development
```sh
$ lein duct setup
```

## Usage

To build the application

```shell
$ lein clean
$ lein uberjar
```

To run the application

```shell
$ java -jar ./target/echo-server-0.1.0-SNAPSHOT-standalone.jar # run the application with a default port of 3000 and sleep of 0
$ ECHO_SERVER_PORT=3001 ECHO_SERVER_SLEEP=5 java -jar ./target/echo-server-0.1.0-SNAPSHOT-standalone.jar # run the application with a specified port and sleep
```

## REST API

### Post a request to be echoed back

- POST http://localhost:3000/echo
- Content-Type: application/json
- Body
```json
{"game":"Mobile Legends", "gamerID":"GYUTDTE", "points":20}
```

- Response
- Status-code: 200
- Body:
```json
{
  "game": "Mobile Legends",
  "points": 20,
  "gamerID": "GYUTDTE",
  "timestamp": "2022-10-09T20:26:57.864Z",
  "host": "localhost:3000"
}
```

## Contact

Michael Angelo Calimlim `<macalimlim@gmail.com>`
