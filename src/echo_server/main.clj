(ns echo-server.main
  (:gen-class)
  (:require [clj-http.client :as http]
            [clojure.core.async :refer [thread]]
            [clojure.data.json :as json]
            [duct.core :as duct]))

(duct/load-hierarchy)

(def routing-server-url "http://localhost:9000/server")
(def content-type "application/json")

(defn request-to-server
  [method-fn]
  (try
    (let [port            (or (System/getenv "ECHO_SERVER_PORT") 3000)
          echo-server-url (str "http://localhost:" port "/echo")
          body-params     {:url echo-server-url}]
      (method-fn routing-server-url
                 {:body         (json/write-str body-params)
                  :content-type content-type}))
    (catch Exception _
      nil)))

(def remove-from-server (partial request-to-server http/delete))
(def add-to-server (partial request-to-server http/post))

(defn start-add-to-server-thread
  []
  (thread
    (loop []
      (add-to-server)
      (Thread/sleep 10000)
      (recur))))

(defn -main [& args]
  (start-add-to-server-thread)
  (.addShutdownHook (Runtime/getRuntime) (Thread. #(remove-from-server)))
  (let [keys     (or (duct/parse-keys args) [:duct/daemon])
        profiles [:duct.profile/prod]]
    (-> (duct/resource "echo_server/config.edn")
        (duct/read-config)
        (duct/exec-config profiles keys))
    (System/exit 0)))
