(ns echo-server.handler.echo
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [clojure.walk :as walk]
            [duct.logger :as logger]
            [integrant.core :as ig])
  (:import [java.util Date]))

(defmethod ig/init-key :echo-server.handler/echo [_ {:keys [logger sleep]}]
  (fn [{:keys [body-params headers]}]
    (let [timestamp     (.toInstant (Date.))
          host          (:host (walk/keywordize-keys headers))
          json-response (-> body-params
                            walk/keywordize-keys
                            (assoc :timestamp timestamp)
                            (assoc :host host))]
      (Thread/sleep (* sleep 1000))
      (logger/log logger :info ::request json-response)
      [::response/ok json-response])))
